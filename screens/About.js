import { View, Text, TouchableOpacity } from 'react-native'
import React from 'react'
import { SafeAreaView } from 'react-native-safe-area-context';

export default function About({ navigation }) {
  const goToHome = () => {
    navigation.navigate('Home');
  }

  return (
    <SafeAreaView>
      <Text>About Screen</Text>
      <TouchableOpacity onPress={goToHome}>
        <Text>Go To Home</Text>
      </TouchableOpacity>
      
    </SafeAreaView>
  )
}