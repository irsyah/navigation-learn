import { View, Text, TouchableOpacity } from 'react-native'
import React from 'react'
import { SafeAreaView } from 'react-native-safe-area-context';

export default function Detail({ route, navigation }) {
  console.log(route);
  const goToAbout = () => {
    navigation.navigate('About');
  }

  return (
    <SafeAreaView>
      <Text>Detail Screen</Text>
      <Text>{route.params}</Text>
      <TouchableOpacity onPress={goToAbout}>
        <Text>Go To About</Text>
      </TouchableOpacity>
      
    </SafeAreaView>
  )
}