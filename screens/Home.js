import { View, Text, TouchableOpacity } from 'react-native'
import React from 'react'
import { SafeAreaView } from 'react-native-safe-area-context'

export default function Home({ navigation }) {

    const goToDetail = () => {
        navigation.navigate("Detail", "Hello World")
    }

    return(
        <SafeAreaView>
            <Text>Home Screen</Text>
            <TouchableOpacity onPress={ goToDetail }>
                <Text>Go To Detail</Text>
            </TouchableOpacity>
        </SafeAreaView>
    )
}
