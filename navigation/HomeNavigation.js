import { View, Text } from 'react-native'
import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Detail from '../screens/Detail';
import Home from '../screens/Home'

const Stack = createNativeStackNavigator();

export default function HomeNavigation() {
  return (
    <Stack.Navigator initialRouteName='HomeScreen'>
        <Stack.Screen name="Detail" component={Detail} />
        <Stack.Screen name="HomeScreen" component={Home} />
    </Stack.Navigator>
  )
}