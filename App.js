import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import { AntDesign, Fontisto } from '@expo/vector-icons';

import Home from './screens/Home';
import Detail from './screens/Detail';
import About from './screens/About';
import HomeNavigation from './navigation/HomeNavigation';

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Tab.Navigator screenOptions={{ headerShown: false}}>
        <Tab.Screen 
          name="Home" 
          component={HomeNavigation} 
          options={{ 
            tabBarIcon: ({ color, size }) => (<AntDesign name="home" size={24} color="black" />)
          }}/>
        <Tab.Screen 
          name="About" 
          component={About} 
          options={{ tabBarIcon: ({ color, size }) => (<Fontisto name="question" size={24} color="black" />) }}/>
      </Tab.Navigator>

      {/* CONTOH PENERAPAN NATIVE STACK */}
      {/* <Stack.Navigator>
        <Stack.Screen name='Home' component={Home} />
        <Stack.Screen name='About' component={About} options = {{ headerShown: false}} />
        <Stack.Screen name='Detail' component={Detail} />
      </Stack.Navigator> */}
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
